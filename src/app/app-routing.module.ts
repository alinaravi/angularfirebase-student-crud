import { StudentsListComponent } from './students-list/students-list.component';
import { CommonModule } from '@angular/common';
import { EditStudentComponent } from './edit-student/edit-student.component';

import { AddStudentComponent } from './add-student/add-student.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path: '', redirectTo: '/register-student', pathMatch: 'full'},
  {path: 'register-student', component: AddStudentComponent},
  {path: 'view-students', component: StudentsListComponent},
  {path: 'edit-student/:id', component: EditStudentComponent}
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
