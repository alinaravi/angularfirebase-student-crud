// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAf1cabFGRw8CRBkmt8mbhg8oeNNQrMn4E",
    authDomain: "angularfirebase-student-be2ae.firebaseapp.com",
    databaseURL: "https://angularfirebase-student-be2ae.firebaseio.com",
    projectId: "angularfirebase-student-be2ae",
    storageBucket: "angularfirebase-student-be2ae.appspot.com",
    messagingSenderId: "1091694682945",
    appId: "1:1091694682945:web:1e986b278b8d0f353cb7c0",
    measurementId: "G-9EZ7JXCPPW"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
